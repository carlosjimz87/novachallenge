const mongoose = require("mongoose");
const fetch = require("node-fetch");
const Nominee = require("../models/nominations");
const { evaluationCriteria } = require("../_helpers/evaluation");
const mailer = require("../_helpers/mailer");
const PORT = process.env.PORT;

exports.nominations_get_all = (req, res, next) => {
  Nominee.find()
    .select("email explanation involvementScore talentScore _id")
    .exec()
    .then((docs) => {
      const response = {
        count: docs.length,
        nominations: docs.map((nominee) => {
          return {
            email: nominee.email,
            explanation: nominee.explanation,
            involvementScore: nominee.involvementScore,
            talentScore: nominee.talentScore,
            _id: nominee._id,
            request: {
              type: "GET",
              description: "Get this nominee",
              url: "/nominations/" + nominee._id,
            },
          };
        }),
      };

      res.status(200).json(response);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.nominations_get_one = (req, res, next) => {
  const id = req.params.nomineeId;
  Nominee.findById(id)
    .select("email explanation involvementScore talentScore _id")
    .exec()
    .then((nominee) => {
      console.log("From database", nominee);
      if (nominee) {
        res.status(200).json({
          nominee: nominee,
          request: {
            type: "GET",
            description: "Get all the nominations",
            url: "/nominations/",
          },
        });
      } else {
        res.status(400).json({
          message: "No nominations for that ID",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};

exports.nominations_create = (req, res, next) => {
  if (req.referrer && req.referrer.id.length > 0) {
    const nominee = new Nominee({
      _id: new mongoose.Types.ObjectId(),
      email: req.body.email,
      explanation: req.body.explanation,
      involvementScore: req.body.involvementScore,
      talentScore: req.body.talentScore,
      referrer: req.referrer.id, // using the referer from the token
    });
    nominee
      .save()
      .then((result) => {
        res.status(201).json({
          message: "Nomination completed. Wait for the nominee evaluation.",
          nominee: {
            _id: result._id,
            email: result.email,
            referrer: result.referrer,
            involvementScore: result.involvementScore,
            talentScore: result.talentScore,
          },
        });

        let criteria = evaluationCriteria(result);
        let generatedPass = Math.random().toString(36).substring(7);
        if (criteria) {
          var headers = {
            "Content-Type": "application/json",
            Authorization: "Bearer " + req.referrer.token,
          };
          var body = {
            email: result.email,
            password: generatedPass,
          };
          //call create
          fetch(`http://localhost:${PORT}/members/signup/`, {
            method: "post",
            body: JSON.stringify(body),
            headers: headers,
          });
        }

        // sending email for both results of evaluation
        if (process.env.SEND_EMAIL === "YES") {
          console.log("Sending email to:", req.referrer.email);
          mailer
            .sendMail(req.referrer.email, result.email, generatedPass, criteria)
            .catch(function (error) {
              console.info(error);
            });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({ message: err.message });
      });
  } else {
    res.status(400).json({
      message:
        "No referrer for this nominee. Please, login again or contact support if problem persists.",
    });
  }
};

// exports.nominations_eval = (req, res, next) => {
//   const id = req.params.nomineeId;

//   Nominee.findById(id)
//     .select("email explanation involvementScore talentScore referrer _id")
//     .exec()
//     .then((nominee) => {
//       console.log("Nominee evaluation for: ", nominee);
//       if (nominee) {
//       } else {
//         res.status(400).json({
//           message: "No nominations for that ID",
//         });
//       }
//     })
//     .catch((err) => {
//       console.log(err);
//       res.status(500).json({ error: err.message });
//     });
// };

exports.nominations_update = (req, res, next) => {
  const id = req.params.nomineeId;
  const updateOps = {};
  for (const ops of req.body) {
    // validate max length
    if (ops.value.length <= 180) updateOps[ops.propName] = ops.value;
  }
  console.log(updateOps);
  Nominee.updateOne({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      if (parseInt(result.nModified) > 0) {
        res.status(200).json({
          message: "Nominee info was updated",
          request: {
            type: "GET",
            description: "Get this nominee",
            url: "/nominations/" + id,
          },
        });
      }
      {
        res.status(400).json({
          message: "Nothing was updated. Invalid params or repeated values.",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};

exports.nominations_delete = (req, res, next) => {
  const id = req.params.nomineeId;

  Nominee.findByIdAndDelete({ _id: id })
    .exec()
    .then((result) => {
      console.log(result);
      if (result) {
        res.status(200).json({
          message: "Nominee deleted",
          request: {
            type: "POST",
            description: "Nominate a new member",
            url: "/nominations/",
            body: {
              email: "String (required-unique)",
              explanation: "String",
              involvementScore: "Number",
              talentScore: "Number",
            },
          },
        });
      } else {
        res.status(400).json({
          message: "No nominations for that ID",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};
