const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
// const jwt = require("jsonwebtoken");
const Auth = require("../_helpers/auth");
const Role = require("../models/role");
const Member = require("../models/members");

exports.members_get_all = (req, res, next) => {
  Member.find()
    .select("email role _id")
    .exec()
    .then((docs) => {
      console.log(docs);
      const response = {
        count: docs.length,
        members: docs.map((member) => {
          return {
            email: member.email,
            role: member.role,
            _id: member._id,
          };
        }),
      };

      res.status(200).json(response);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};

exports.members_signup = (req, res, next) => {
  console.log("SIGNING UP", req.body.email);

  // check if member already exists in network
  Member.find({ email: req.body.email })
    .exec()
    .then((member) => {
      if (member.length >= 1) {
        return res.status(409).json({
          message: "Member already exists",
        });
      } else {
        // check if member was nominated and accepted
        //TODO

        // encrypt password
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err.message,
            });
          } else {
            // create the new member with hashed password
            const newMember = new Member({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              role: Role.Member, // TODO admin creation still pending
              password: hash,
            });
            newMember
              .save()
              .then((result) => {
                console.log(result);
                res.status(201).json({
                  message: "New member created",
                });
              })
              .catch((err) => {
                console.log(err);
                res.status(500).json({ error: err.message });
              });
          }
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};

exports.members_login = (req, res, next) => {
  Member.find({ email: req.body.email })
    .exec()
    .then((member) => {
      if (member.length < 1) {
        return res.status(401).json({
          message: "Authentication failed",
        });
      }
      bcrypt.compare(req.body.password, member[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "Authentication failed",
          });
        }
        if (result) {
          const token = Auth.signToken(member);

          return res.status(200).json({
            message: "Member authenticated",
            token: token,
            role: member[0].role,
          });
        }
        res.status(401).json({
          message: "Authentication failed",
        });
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};

exports.members_delete = (req, res, next) => {
  const id = req.params.memberId;

  Member.findByIdAndDelete({ _id: id })
    .exec()
    .then((result) => {
      console.log(result);
      if (result) {
        res.status(200).json({
          message: "Member deleted",
        });
      } else {
        res.status(400).json({
          message: "No member for that ID",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.message });
    });
};
