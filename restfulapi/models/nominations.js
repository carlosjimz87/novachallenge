const mongoose = require("mongoose");

const nomineeSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: {
    type: String,
    required: true,
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  explanation: { type: String, maxlength: 180 },
  involvementScore: { type: Number, required: true, max: 10, min: 0 },
  talentScore: { type: Number, required: true, max: 10, min: 0 },
  referrer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Member",
    required: false,
  },
});

module.exports = mongoose.model("Nominee", nomineeSchema);
