const express = require("express");
const router = express.Router();

const MemberController = require("../controllers/members");
router.get("/", MemberController.members_get_all);

router.post("/signup", MemberController.members_signup);

router.post("/login", MemberController.members_login);

router.delete("/:memberId", MemberController.members_delete);

module.exports = router;
