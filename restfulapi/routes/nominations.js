const express = require("express");
const router = express.Router();
const NominationsController = require("../controllers/nominations");

router.get("/", NominationsController.nominations_get_all);
router.post("/", NominationsController.nominations_create);
router.get("/:nomineeId", NominationsController.nominations_get_one);
router.patch("/:nomineeId", NominationsController.nominations_update);
// router.post("/eval/:nomineeId", NominationsController.nominations_eval);
router.delete("/:nomineeId", NominationsController.nominations_delete);

module.exports = router;
