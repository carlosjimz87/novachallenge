const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();

// including middleware
const authCheck = require("./middleware/authCheck");
// including routes
const nominationsRoute = require("./routes/nominations");
const membersRoute = require("./routes/members");

// adding loging middleware
app.use(morgan("dev"));
// adding auth middleware
app.use(authCheck);
// adding bodyparsers
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// disabling CORS policy
app.use(cors());

// adding routes
app.use("/nominations", nominationsRoute);
app.use("/members", membersRoute);

// adding default route
app.use((req, res, next) => {
  const error = new Error("Invalid endpoint.");
  error.status = 404;
  next(error);
});
// adding rest of errors
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

module.exports = app;
