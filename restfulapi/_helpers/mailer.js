const nodemailer = require("nodemailer");
let date = new Date(Date.now());

function mailToNominee(from = "member@nova.es", to, newPass, status) {
  return {
    from: from,
    to: to,
    subject: "Nova Spain",
    text: `Hello,\n\nThe Nova Talent Network informs that the nominee ${to} proposed by our member ${from}, has been ${
      status
        ? `ACCEPTED. Congratulations! You can now to login to the network with this password:\n${newPass}\n`
        : "REJECTED. We are sorry for this result, and we encourage the nominee to try again later."
    } \n\n
    Best regards,\n
    The Global Talent Manager at Nova Talent\n
    ${date}`,
  };
}

function mailToReferrer(from = "member@nova.es", to, status) {
  return {
    from: from,
    to: to,
    subject: "Nova Spain",
    text: `Hello,\n\nThe Nova Talent Network informs that the nominee proposed by YOU, has been ${
      status ? "ACCEPTED." : "REJECTED."
    } \n\n
          Best regards,\n
          The Global Talent Manager at Nova Talent\n
          ${date}`,
  };
}

module.exports.sendMail = function (
  from = "member@nova.es",
  to,
  newPass,
  status
) {
  let transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS,
    },
  });

  return (
    new Promise(function (resolve, reject) {
      transporter.sendMail(mailToNominee(from, to, newPass, status), function (
        error,
        info
      ) {
        if (error) {
          reject(error);
        } else {
          resolve(info);
        }
      });
    }) &&
    new Promise(function (resolve, reject) {
      transporter.sendMail(mailToReferrer(null, from, status), function (
        error,
        info
      ) {
        if (error) {
          reject(error);
        } else {
          resolve(info);
        }
      });
    })
  );
};
