module.exports.evaluationCriteria = function (nominee) {
  return nominee.talentScore >= 8;
};
