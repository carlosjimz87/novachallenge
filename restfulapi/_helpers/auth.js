const jwt = require("jsonwebtoken");

module.exports.decodeToken = (token) =>
  jwt.verify(token, process.env.JWT_KEY, function (error, decoded) {
    if (error) return error;
    else return decoded;
  });

module.exports.signToken = (member) =>
  jwt.sign(
    {
      email: member[0].email,
      userId: member[0]._id,
      role: member[0].role,
    },
    process.env.JWT_KEY,
    {
      expiresIn: "1h",
    }
  );
