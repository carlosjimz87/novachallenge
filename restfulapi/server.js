const http = require("http");
const mongoose = require("mongoose");

const port = process.env.PORT || 3000;
const app = require("./app");

const server = http.createServer(app);

const uri = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.mg8oj.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`;

mongoose
  .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    server.listen(port);
    console.log("Server running on port ", port);
  })
  .catch((err) => {
    console.log(err);
  });
