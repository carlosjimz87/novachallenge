const Role = require("../models/role");
const Auth = require("../_helpers/auth");

module.exports = (req, res, next) => {
  //login is open to any
  if (req.path !== "/members/login") {
    if (req.headers.authorization) {
      // get token from headers
      let token = req.headers.authorization.split(" ")[1];
      // verify token by decrypting with the key
      let decoded = Auth.decodeToken(token);
      // if token is secured and correct...
      if (decoded) {
        //login and admin has access to POST /nominations (Admins could be forbidden for this endpoint)
        if (req.method === "POST" && req.path === "/nominations/") {
          // pass the referrer obtained from the decoded token
          req.referrer = {
            id: decoded.userId,
            email: decoded.email,
            token: token,
          };
          // req.emailReferrer = decoded.email;
          next();
        } else {
          // if not, all the rest of endpoints need Admin access
          if (decoded.role === Role.Admin) next();
          else res.status(403).send({ error: "Unauthorized service" });
        }
      } else {
        return res
          .status(403)
          .send({ error: "Unauthorized service", message: error.message });
      }
    } else res.status(403).send({ error: "Unauthorized service" });
  } else next(); // login is open for any user
};
