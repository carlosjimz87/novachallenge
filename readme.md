# RESTful Api

RESTful Api Demo is a simple REST Api to demonstrate the basic principles of this technology.

## TO-DO

### Endpoints

- _propose_ - endpoint to propose nominations of members by other members. [user]
  Params: - Peer's email

  - Nomination explanation - Involvement Score (1-10) - Overall Talent Score (1-10)

- _result_ - endpoint to notify the result of a nomination. [user]
  Response [NominationObject]: - userId

  - result - explanation - referrer

- _list_ - endpoint to list all succesfull nominations. [admin]
  Response: - ...NominationObject

### Internals

- _acceptance policy_ - To accept memebers in the network is needed an Overall Talent Score >= 8.
- _acceptance actions_ - After a member is accepted send an email to nominated and referrer with the result.

## INSTALLING

Just use npm installation like this:

```
npm install
```

## DEPLOYMENT

For local deployment:

```
npm start
```

## REQUIREMENTS

[X] - Nova members can nominate their peers (with email, explanation, talent score (0-10) and involvement score (1-10)
[X] - Repeated nominations (by email) will not be saved. Rest of them do.
[X] - Stored nominations with a talent score < 8 are automatically rejected. This means that these nominees are not saved as members. Rest of them do.
[X] - All rejections should trigger an email to both the referrer and the candidate explaining the rejection.
[X] - An admin can list all the non-rejected nominations, including who is the referring Nova member.
[X] - Use roles in members for authorization access.

## EXTRAS

[ ] - Create a ReactJS or Swagger frontend.
[ ] - Config dev & prod environments to change database connection
[ ] - Create Unit and Integration Tests.
[ ] - Refactoring and cleanup, replace promises for async/await to avoid nested promises.
[ ] - Use a more robust data type for roles.
[ ] - Define a secure approach with CREATE and EVAL endpoints of /nominations. Both are very similar.
[ ] - Improve the approach of chained endpoints to evaluate nominees and send emails.
[ ] - Perform cascade deletion for members and its nominations.
[ ] - Protect critical data changes in members (only allow to change passwords)
[ ] - Implement default loaded data in a database such as admins or special members.
[ ] - Deploy with heroku

## BUILT WITH

- [Npm](https://docs.npmjs.com/) - for dependency management.
- [Express](http://expressjs.com/es/starter/installing.html) - for fast, unopinionated, minimalist web framework.
- [Nodemailer](https://nodemailer.com/about/) - o allow easy as cake email sending.
- [Mongoose](https://mongoosejs.com/) - to create elegant mongodb object modeling.

## AUTHOR

- **Carlos Jiménez** : [carlosjimz87](https://github.com/carlosjimz87)

## LICENSE

This project is licensed under the ISC License - see the [LICENSE.md](LICENSE.md) file for details
